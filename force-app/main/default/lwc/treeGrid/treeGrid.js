/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable vars-on-top */
import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import gettreeData from '@salesforce/apex/GetAllContactsAccount.gettreeData';
 
export default class TreeGrid extends LightningElement {
    @track columns = [{
            type: 'radio',
            fieldName: 'name',
            label: 'name'
        },
        {
            type: 'radio',
            fieldName: 'label',
            label: 'label'
        }
    ];
     @api recordId;
     @track treeItems;
     @track error;
     @track recordId='{$recordId}';
     //USE WIRE TO INVOKE DIRECTLY
   // @wire(gettreeData)
   /** 
    *  wireTreeData({
         error,
         data
     }) {
         if (data) {
             //  alert(data);
             var res = data;
             console.log("Response "+res);

             var tempjson = JSON.parse(JSON.stringify(data).split('items').join('_children'));
             console.log("Response1="+tempjson);
             this.treeItems = tempjson;
             console.log(JSON.stringify(tempjson, null, '\t'));
         } else {
             this.error = error;
             //  alert('error' + error);
         }
     }
*/ 
// ON BUTTON CLICK
     handleLoad(){
        gettreeData()
          .then(result=>{
            var tempjson = JSON.parse(JSON.stringify(result).split('items').join('_children'));
            console.log("Response1="+tempjson);
            this.treeItems=tempjson;
          })
          .catch(error=>{
              this.error=error;
          })
     }
}

