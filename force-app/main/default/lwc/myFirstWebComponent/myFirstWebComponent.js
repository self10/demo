/* eslint-disable radix */
import { LightningElement,track } from 'lwc';
//import strUserId from '@salesforce/user/Id'
export default class MyFirstWebComponent extends LightningElement {
    @track firstNumber;
    @track secondNumber;
    @track currentResult;
    @track operatorUsed;
    onNumberChange(event){
        const inputFieldName=event.target.Name;
        if(inputFieldName==="firstName"){
            this.firstNumber=event.target.value;
        }else if(inputFieldName==="secondName"){
            this.secondNumber=event.target.value;
        }
    }
    onAdd(){
         // eslint-disable-next-line radix
         this.currentResult = parseInt(this.firstNumber) + parseInt(this.secondNumber);
         this.operatorUsed = '+';
    }
    onSub() {
        this.currentResult = parseInt(this.firstNumber) - parseInt(this.secondNumber);
        this.operatorUsed = '-';
        //Check if both numbers are okay and operable 
      }
      
    onMultiply() {
        this.currentResult = parseInt(this.firstNumber) * parseInt(this.secondNumber);
        this.operatorUsed = '*';
        //Check if both numbers are okay and operable 
      }
     
    onDivide() {
        this.currentResult = parseInt(this.firstNumber) / parseInt(this.secondNumber);
        this.operatorUsed = '%';
        //Check if both numbers are okay and operable 
      }

    get result() {
    
        if(this.currentResult === 0 || this.currentResult){
          return `Result of ${this.firstNumber} ${this.operatorUsed} ${this.secondNumber} is ${this.currentResult}`;
        // eslint-disable-next-line no-else-return
        } else{
          return '';
        }
      }
    
}